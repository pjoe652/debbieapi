require('dotenv').config();
const scheduledRoutes = require('./scheduledRoutes')
const geolib = require('geolib')

/* 
	Filters planned and scheduled jobs and calculates the best route for the planned jobs 
	@params {Object} fetch - Node-fetch package
	@params {Object[]} storedJobs - Stored jobs to be processed and filtered
	@params {String} moment - Moment.js package
	@params {Boolean} demo - Boolean to determine whether demo parameters are used
	@params {Number} leniancy - Number of minutes allowed as leniancy
	@params {Object} jsgraphs - js-graph-algorithms package
	@params {Object} req - Input JSON to API
	@params {Object} res - Output JSON from API
	@params {String} user - User Id
	@params {Object} userLocation - Location of user as latitude and longitude
	@params {Boolean} userRestrict - Boolean to determine whether to allow any assigned jobs to user
	@params {Boolean} locationRestrict - Boolean to determine whether to allow any job outside of range
	@returns {Promise} Returns promise which resolves the ideal path
*/

	function createRoutes(fetch, storedJobs, moment, demo, leniancy, jsgraphs, req, res, user, userLocation, userRestrict, locationRestrict, defaultWorkTime, defaultGapTime, routeChoice, radius) {

	// Status numbers
	let totalJobSize = storedJobs.length;
	let withinRangeSize = 0;
	let routesPossible = 0;
	let scheduledJobCount = 0;

	// Initialising the start and end nodes to restrict jobs within the day
	const start =  new moment()
	let minute = start.get('minute')
	let hour = start.get('hour')
	let day = start.get('date')
	let month = start.get('month')+1
	const year = start.get('year')

	if(minute < 10) {
		minute = `0${minute}`
	}

	if(hour < 10) {
		hour = `0${hour}`
	} 

	if(day < 10) {
		day = `0${day}`
	}

	if(month < 10) {
		month = `0${month}`
	}

	if(demo) {
		day = '01';
		month = '02';
	}

	// console.log(`${year}-${month}-${day}T${hour}:${minute}`)

	let startRestrict;
	let endRestrict;
	if (demo) {
		// startRestrict = new moment(`2019-01-28 09:00`, `YYYY-MM-DD HH:mm`)
		// endRestrict = new moment(`2019-01-28 17:00`, `YYYY-MM-DD HH:mm`)
		startRestrict = new Date(`2019-01-28T09:00`)
		endRestrict = new Date(`2019-01-28T17:00`)
	} else {
		startRestrict = new Date(`${year}-${month}-${day}T${hour}:${minute}`)
		endRestrict = new Date(Date.UTC(year, month-1, day-1, 20, 0, 0));
		endRestrict.setTime(endRestrict.getTime() + (8 * 60 * 60000))
		// endRestrict = new moment(`${year}-${month}-${day}T${'09'}:${'00'} +0000`, "YYYY-MM-DD HH:mm Z")

		// endRestrict.add(8, 'hours')
	}
	console.log(`${year}-${month}-${day}T${'09'}:${'00'} +0000`)

	// console.log(startRestrict)
	// console.log(endRestrict)
	// console.log(startRestrict.toISOString())
	// console.log(endRestrict.toISOString())

		const startNode = {
			id : "StartNode",
			plannedFrom : startRestrict.toISOString(),
			plannedTo : startRestrict.toISOString(),
			assignedUserId : null,
			jobLocations : [{
				gps: {
					// Peter Joe Location
					latitude: userLocation.latitude,
					longitude: userLocation.longitude,
				},
			}],
			earlyFactor: 0,
		}

		const endNode = {
			id : "EndNode",
			plannedFrom : endRestrict.toISOString(),
			plannedTo : endRestrict.toISOString(),
			assignedUserId : null,
			jobLocations : [{
				gps: {
					// Peter Joe Location
					latitude: userLocation.latitude,
					longitude: userLocation.longitude,
				}
			}],
			earlyFactor: 0
		}

		console.log(startNode)
		console.log(endNode)

		let allJobs;

		// Check whether jobs lie withing a circle radius

		if(locationRestrict){
			allJobs = storedJobs.filter(job => {
				try {
					const jobLocation = {latitude : parseFloat(job.jobLocations[0].gps.latitude),
						longitude : parseFloat(job.jobLocations[0].gps.longitude)}
						if(geolib.isPointInCircle(jobLocation,userLocation, radius*1000)) {
							return true;
						} else {
							return false
						}}
						catch(e) {
							// console.log(`${job.id} is invalid as it is not within range`)
						}
					})
		} else {
			allJobs = storedJobs
		}

		withinRangeSize = allJobs.length;

		const scheduledJobs = []
		const assignedJobs = []


		// Convert database jobs into UTC and seperate jobs
		allJobs = allJobs.filter(job => {
			job.plannedFrom = job.plannedFrom + ".000Z";
			job.plannedTo = job.plannedTo + ".000Z";

			if (job.assignedUserId === user && new Date(job.plannedFrom) < new Date(endNode.plannedFrom) 
				&& new Date(job.plannedFrom) > new Date(startNode.plannedFrom)) {
				assignedJobs.push(job)
			return true 
		} else if (new Date(job.plannedFrom) <= new Date(endNode.plannedFrom) 
			&& new Date(job.plannedFrom) >= new Date(startNode.plannedFrom)) {
			return true;
		} else if (job.plannedFrom === job.plannedTo){
			scheduledJobs.push(job)
			return false
		}
	})

		scheduldeJobCount = scheduledJobs.length;

		// Manually insert start nodes and end nodes
		assignedJobs.unshift(startNode)
		assignedJobs.push(endNode)

		// Sorting all jobs in order in terms of date
		assignedJobs.sort(function(a, b){
			const aDate = new Date(a.plannedFrom)
			const bDate = new Date(b.plannedFrom)
			return aDate - bDate;
		})

		allJobs.sort(function(a, b) {
			const aDate = new Date(a.plannedFrom)
			const bDate = new Date(b.plannedFrom)
			return aDate - bDate;
		})

		allJobs.forEach((job, i) => {
			allJobs[i].earlyFactor = i+1;
		})

		allJobs.push(startNode)
		allJobs.push(endNode)

		// Check all paths between jobs, whether they're before other jobs
		const jobRoutes = {}
		const LatLngRoutes = {}

		if(userRestrict) {
			allJobs.forEach(job => {
				// Allow pairing if job is not a scheduled jobs, the origin is not EndNode, and if job assignedUserID is either the user or null
				if((job.plannedFrom !== "null.000Z" || job.plannedTo !== "null.000Z") && job.id !== "EndNode" 
					&& (job.assignedUserId === user || job.assignedUserId === null)
					) {
					let jobLatitude;
				let jobLongitude;

				try {
					jobLatitude = job.jobLocations[0].gps.latitude;
					jobLongitude = job.jobLocations[0].gps.longitude;
					allJobs.forEach(searchedJob => {
						
						let searchedJobLatitude;
						let searchedJobLongitude;
						let workTime;
						let gapTime;

						try {
							searchedJobLatitude = searchedJob.jobLocations[0].gps.latitude;
							searchedJobLongitude = searchedJob.jobLocations[0].gps.longitude;
							// Allow pairing if destination job starts after origin job ends and if the destination job assignedUserId is either the user or null
							if (new Date(searchedJob.plannedFrom) > new Date(job.plannedTo) && job.id !== searchedJob.id 
								&& (searchedJob.assignedUserId === user || searchedJob.assignedUserId === null)
								) {
								try{
									workTime = moment(new Date(searchedJob.plannedTo)).diff(moment(new Date(searchedJob.plannedFrom)), 'minutes')
									gapTime = moment(new Date(searchedJob.plannedFrom)).diff(moment(new Date(job.plannedTo)), 'minutes')
									jobRoutes[job.id][searchedJob.id] = {
										endPos : `${searchedJob.jobLocations[0].gps.latitude},${searchedJob.jobLocations[0].gps.longitude}`,
										workTime : workTime,
										gapTime : gapTime,
										earlyFactor : searchedJob.earlyFactor
									};
								} catch(e) {
									workTime = moment(new Date(searchedJob.plannedTo)).diff(moment(new Date(searchedJob.plannedFrom)), 'minutes')
									gapTime = moment(new Date(searchedJob.plannedFrom)).diff(moment(new Date(job.plannedTo)), 'minutes')
									jobRoutes[job.id] = {
										startPos : `${job.jobLocations[0].gps.latitude},${job.jobLocations[0].gps.longitude}`
									};
									jobRoutes[job.id][searchedJob.id] = {
										endPos : `${searchedJob.jobLocations[0].gps.latitude},${searchedJob.jobLocations[0].gps.longitude}`,
										workTime : workTime,
										gapTime : gapTime,
										earlyFactor : searchedJob.earlyFactor
									};
								}
							}
						} catch(e) {
							// console.log(`${searchedJob.id} has an invalid location`)
						}
					})
				} catch(e) {
					// console.log(`${job.id} has an invalid location`)
				}
			} else {
				if (job.id !== "EndNode" && job.assignedUserId === null) {
					scheduledJobs.push(job)
				}
			}
		})
		} else {
			allJobs.forEach(job => {
				// Allow pairing if job is not a scheduled jobs, the origin is not EndNode
				if((job.plannedFrom !== "null.000Z" || job.plannedTo !== "null.000Z") && job.id !== "EndNode") {
					let jobLatitude;
					let jobLongitude;
					try {
						jobLatitude = job.jobLocations[0].gps.latitude;
						jobLongitude = job.jobLocations[0].gps.longitude;
						allJobs.forEach(searchedJob => {
							
							let searchedJobLatitude;
							let searchedJobLongitude;
							let workTime;
							let gapTime;

							try {
								searchedJobLatitude = searchedJob.jobLocations[0].gps.latitude;
								searchedJobLongitude = searchedJob.jobLocations[0].gps.longitude;
								// Allow pairing if destination job starts after origin job ends
								if (new Date(searchedJob.plannedFrom) > new Date(job.plannedTo) && job.id !== searchedJob.id) {
									try{
										workTime = moment(new Date(searchedJob.plannedTo)).diff(moment(new Date(searchedJob.plannedFrom)), 'minutes')
										gapTime = moment(new Date(searchedJob.plannedFrom)).diff(moment(new Date(job.plannedTo)), 'minutes')
										jobRoutes[job.id][searchedJob.id] = {
											endPos : `${searchedJob.jobLocations[0].gps.latitude},${searchedJob.jobLocations[0].gps.longitude}`,
											location: `${searchedJob.jobLocations[0].city},${searchedJob.jobLocations[0].country}`,
											workTime : workTime,
											gapTime : gapTime,
											earlyFactor : searchedJob.earlyFactor
										};
										// possibleRoutesBeforeGoogleAPI++
									} catch(e) {
										workTime = moment(new Date(searchedJob.plannedTo)).diff(moment(new Date(searchedJob.plannedFrom)), 'minutes')
										gapTime = moment(new Date(searchedJob.plannedFrom)).diff(moment(new Date(job.plannedTo)), 'minutes')
										jobRoutes[job.id] = {
											startPos : `${job.jobLocations[0].gps.latitude},${job.jobLocations[0].gps.longitude}`
										};
										jobRoutes[job.id][searchedJob.id] = {
											endPos : `${searchedJob.jobLocations[0].gps.latitude},${searchedJob.jobLocations[0].gps.longitude}`,
											location: `${searchedJob.jobLocations[0].city},${searchedJob.jobLocations[0].country}`,
											workTime : workTime,
											gapTime : gapTime,
											earlyFactor : searchedJob.earlyFactor
										};
										// possibleRoutesBeforeGoogleAPI++;
									}
								}
							} catch(e) {
								// console.log(`${searchedJob.id} has an invalid location`)
							}
						})
					} catch(e) {
						// console.log(`${job.id} has an invalid location`)
					}
				} else {
					if (job.id !== "EndNode" && job.assignedUserId === null) {
						scheduledJobs.push(job)
					}
				}
			})
		}



		// Find out how many requests need to be made, if it exceeds 50 (Google API rate limit value) then switch to street search
		let requestCount = 0
		for(let key in jobRoutes) {
			requestCount += Math.ceil((Object.keys(jobRoutes[key]).length-1)/25)
		}

		// Use Google API, where we can insert 25 destinations/25 origins at a time
		const urlObject = {};
		const streetObject = {};
		const streetNames = [];


		for(let key in jobRoutes) {
			const urlRequests = []
			const searchedJobIds = (Object.keys(jobRoutes[key]))
			let count = 0;
			let index = 0;

			let url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${jobRoutes[key]['startPos']}&destinations=`
			searchedJobIds.forEach(id => {
				if (id !== 'startPos') {
					count++;
					url += `${jobRoutes[key][id]['endPos']}|`
				}
				if (count === 24) {
					index++;
					count = 0 ;
					url += `&traffic_model=best_guess&departure_time=now&key=${process.env.GOOGLE_KEY}`;
					urlRequests.push(url)
					url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${jobRoutes[key]['startPos']}&destinations=`;
				}
		})
			url += `&traffic_model=best_guess&departure_time=now&key=${process.env.GOOGLE_KEY}`;

			urlRequests.push(url)

			urlObject[key] = {
				count : index * 24 + count,
				urlRequests : urlRequests
			}
		}

		// Adding edges and secondary check whether jobs are able to reach another job, this time taking travel time into account

		const promiseList = [];
		const plannedJobTravel = {};

		const jobArrangement = Object.keys(jobRoutes);
		jobArrangement.push("EndNode")

		// Create graph for Bellman Ford algorithm

		const graph = new jsgraphs.WeightedDiGraph(storedJobs.length);

		/*
			urlObject : 

			jobIdA : {
				startPos: lat,lng,
				count: num,
				urlRequests: ['longstring of urls', 'longstring of urls'] 
			}
			jobIdB : {
				startPos: lat,lng,
				count: num,
				urlRequests: ['longstring of urls', 'longstring of urls'] 
			}
			...
		*/

		/*
			jobRoutes :

			jobIdA : {
				startPost: lat,lng
				jobIdB : {
					gapTime: num,
					workTime : num,
				},
				jobIdC : {
					gapTime: num,
					workTime : num,
				}
			}
		*/

		for(let origin in urlObject) {
			let count = 0;
			const objCount = urlObject[origin]['count'];
			const urls = urlObject[origin]['urlRequests'];
			const searchedIds = Object.keys(jobRoutes[origin])

			urls.forEach(url => {
				const request = fetch(url, {
					method : "POST",
					headers : { "content-type" : "application/json" } 
				})
				.then(results => {
					return results.json()
				})
				.then(results => {
					if (results.status === 'OK') {
						const routes = results['rows'][0]['elements']
						routes.forEach((items, i) => {
							const destination = searchedIds[count+1]
							
							const relatedObj = jobRoutes[origin][destination]
							const gapTime = relatedObj.gapTime;
							const earlyFactor = relatedObj.earlyFactor;
							count++;

							if (items.status !== "OK") {

							} else if (gapTime !== 0) {
								// Round job to the nearest minute
								let travelTime = ((items.duration_in_traffic.value)/60);
								if (travelTime < 0.5) {
									travelTime = 1;
								} else {
									travelTime = Math.round(travelTime)
								}

								// Check whether travel time is less than the gap time plus the leniancy value
								const leniancyValue =  gapTime + leniancy;

								if (leniancyValue < travelTime && destination !== "EndNode") {
									// console.log(`${origin} cannot make it to ${destination} in time`)
								} else { 
									try{
										const fromJob = jobArrangement.indexOf(origin)
										const toJob = jobArrangement.indexOf(destination)
										const earlyMultiplier = 1 + ((jobArrangement.length-earlyFactor)/100)
										if (earlyMultiplier !== NaN) {
											console.log(earlyMultiplier)
											const weighting = relatedObj.workTime * (-1) * earlyMultiplier
											graph.addEdge(new jsgraphs.Edge(fromJob, toJob, weighting));
										}

										routesPossible++;
									} catch(e) {
										// console.log(e)
									}
								}
							}
						})
					}
				})
				promiseList.push(request)
			})
		}

		// Determine the plannedRoute (only jobs with valid plannedTo or plannedFrom) taking into account the user's assigned jobs
		Promise.all(promiseList).then(result => {

			const plannedPath = [];
			const plannedPathObjs = [];
			const startIndex = jobArrangement.indexOf("StartNode")
			const endIndex = jobArrangement.indexOf("EndNode")

				/*
				Assigned Jobs holds jobs that the user must do as it's already assigned to them, the path is taken step by step to force the
				user to take a path that takes into account their schedule, if there's nothing then it will just find the best path from
				startNode to endNode
				*/

				let error = false;
				assignedJobs.forEach((job, i) => {

					try {
						if (job.id !== "EndNode") {
							const jobA = assignedJobs[i].id;
							const jobB = assignedJobs[i+1].id;
							const jobAIndex = jobArrangement.indexOf(jobA)
							const jobBIndex = jobArrangement.indexOf(jobB)

							const bellmanGraph = new jsgraphs.BellmanFord(graph, jobAIndex);
							const path = bellmanGraph.pathTo(jobBIndex);
							path.forEach(item => {
								plannedPath.push(jobArrangement[item.from()])
							})
						}
					} catch(e){
						// console.log(e)
						error = true;
					}
				})
				plannedPath.push("EndNode");

			// Error checking
			console.log(assignedJobs)
			if(error) {
				console.log("\x1b[31m","ERROR: Error to find jobs given location")
				console.log("\x1b[0m")
				const returnObj = {
					status_message : "Error to find jobs given location",
					recommended_path : [],
					assigned_path: assignedJobs,
					startRestrict: startRestrict,
					endRestrict: endRestrict,
					assembly: `${year}-${month}-${day}T${hour}:${minute}`,
					assembly2: `${year}-${month}-${day}T${17}:${00}`
				}
				res.status(400).json(returnObj)
			} else {
				
				allJobs.forEach(job => {
					if(plannedPath.includes(job.id)) {
						plannedPathObjs.push(job)
					}
				})
				
				plannedPathObjs.sort(function(a, b){
					const aDate = new Date(a.plannedFrom)
					const bDate = new Date(b.plannedFrom)
					return aDate - bDate;
				})

				// Pass onto schedule route planning
				scheduledRoutes.createRoutes(fetch, scheduledJobs, plannedPathObjs, jsgraphs, moment, leniancy, res, req, totalJobSize, withinRangeSize, routesPossible, scheduledJobCount, defaultWorkTime, defaultGapTime, routeChoice)

			}
		})
		.catch((err) => {
			console.log("\x1b[31m","ERROR: Invalid User Location")
			console.log("\x1b[0m")
			const returnObj = {
				status_message : "Invalid User Location",
			}
			res.status(400).json(returnObj)
		});
}

module.exports = {
	createRoutes : createRoutes
}