require('dotenv').config();


// Values from https://openweathermap.org/weather-conditions

function weatherMultiplier (locations, fetch) {

	const promiseReturn = new Promise(resolve => {
		
		const multiplier = [];
		const promiseList = [];

		
		locations.forEach((location, i) => {
			let multiplierObj;
			
			const promise = fetch(`http://api.openweathermap.org/data/2.5/weather?q=${location}&APPID=${process.env.WEATHER_KEY}`,
				{method : "GET"}
				).then(result => {
					return result.json()
				}).then(result => {
					try {
						const weatherCode = Math.floor(result.weather[0].id/100)*100
						console.log(weatherCode)
						switch(weatherCode) {
							
						// Thunderstorm
						case 200 :
						multiplierObj = {
							location : location,
							multiplier : 1.5}
							multiplier.push(multiplierObj)
							break;
						// Drizzle
						case 300 : 
						multiplierObj = {
							location : location,
							multiplier : 1.2}
							multiplier.push(multiplierObj)
							break;
						// Rain
						case 500 : 
						multiplierObj = {
							location : location,
							multiplier : 1.3}
							multiplier.push(multiplierObj)
							break;
						// Snow
						case 600 : 
						multiplierObj = {
							location : location,
							multiplier : 1.4}
							multiplier.push(multiplierObj)
							break;
						// Atmosphere
						case 700 : 
						multiplierObj = {
							location : location,
							multiplier : 1.2}
							multiplier.push(multiplierObj)
							break;
						// Cloudly or clear sky
						case 800 :
						multiplierObj = {
							location : location,
							multiplier : 1}
							multiplier.push(multiplierObj)
							break;
							default :
							multiplierObj = {
								location : location,
								multiplier : 1}
								multiplier.push(multiplierObj)
								break;
							}}
							catch(e){
								multiplierObj = {
									location : location,
									multiplier : 1}
									multiplier.push(multiplierObj)
								}
								
							})
				promiseList.push(promise)
			})
		Promise.all(promiseList).then(result => {
			resolve(multiplier)
		})
	})
	return promiseReturn
}

module.exports = {
	weatherMultiplier : weatherMultiplier
}