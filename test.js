const fetch = require('node-fetch');
require('dotenv').config();
var googleMapsClient = require('@google/maps').createClient({
	key: process.env.GOOGLE_KEY,
	Promise: Promise
});

const singleRequest = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=-39.93730170000001,
174.987536&destinations=-37.2827744,175.3030677|-37.694919,176.106225&key=${process.env.GOOGLE_KEY}`

const requestArray = []
const promiseArray = []

for(let i = 0; i < 300; i++) {
	requestArray.push(singleRequest)
}

requestArray.forEach(request => {
	const requestPromise = fetch(request, {
		method: "POST",
		headers: {'Content-Type' : 'Application/json'}
	})
	.then(result => {
		return result.json()
	})
	.then(result => {
		console.log(result)
	})
	promiseArray.push(requestPromise)
})

Promise.all(promiseArray).then(result => {
	console.log("Done!")
})