require('dotenv').config();


/* 
	Filters planned and scheduled jobs and calculates the best route for the planned jobs 
	@params {Object} fetch - Node-fetch package
	@params {Object[]} scheduledJobs - Scheduled jobs to be processed and filtered
	@params {Object[]} plannedJobs - Planned jobs to be processed and filtered (The best planned job route)
	@params {Object} jsgraphs - js-graph-algorithms package
	@params {String} moment - Moment.js package
	@params {Number} leniancy - Number of minutes allowed as leniancy
	@params {Object} res - Output JSON from API
	@params {Object} req - Input JSON to API
	@params {Number} totalJobSize - Total number of jobs gained from Con-X API to track processing
	@params {Number} withinRangeSize - Number of jobs within range to track processing
	@params {Number} routesPossible - Number of routes calculated during Bellman Ford algorithm to track processing
	@params {Number} scheduledJobCount - Number of schedules jobs gained from Con-X API to track processing
	@returns {Promise} Returns promise which resolves the ideal path
*/

function createRoutes(fetch, scheduledJobs, plannedJobs, jsgraphs, moment, leniancy, res, req, totalJobSize, withinRangeSize, routesPossible, scheduledJobCount, defaultWorkTime, defaultGapTime, routeChoice) {

	// Sort and create array of jobs containing all jobs
	const jobRoutes = {}
	const scheduledAndPlannedJobs = [];
	const plannedJobIds = [];
	scheduledJobs.forEach(job => {
		scheduledAndPlannedJobs.push(job)
	})

	plannedJobs.forEach(job => {
		job.earlyFactor = 1;
		plannedJobIds.push(job.id)
		scheduledAndPlannedJobs.push(job)
	})

	scheduledJobs.sort(function(a, b){
		const aDate = new Date(a.scheduleTo)
		const bDate = new Date(b.scheduleTo)
		return aDate - bDate;
	})

	scheduledJobs.forEach((job, i) => {
		job.earlyFactor = i+1;
	})

	// Check all paths between jobs, scheduled jobs can fit anywhere as long as theres a space
	scheduledAndPlannedJobs.forEach(job => {
		scheduledAndPlannedJobs.forEach(searchedJob => {
			try{
				// Static 45 minute work time and static 10 minute gap time
				workTime = defaultWorkTime 
				jobRoutes[job.id][searchedJob.id] = {
					endPos : `${searchedJob.jobLocations[0].gps.latitude},${searchedJob.jobLocations[0].gps.longitude}`,
					workTime : workTime,
					earlyFactor : searchedJob.earlyFactor,
					gapTime: defaultGapTime
				};
			} catch(e) {
				// Static 45 minute work time and static 10 minute gap time
				workTime = defaultWorkTime
				jobRoutes[job.id] = {
					startPos : `${job.jobLocations[0].gps.latitude},${job.jobLocations[0].gps.longitude}`
				};
				jobRoutes[job.id][searchedJob.id] = {
					endPos : `${searchedJob.jobLocations[0].gps.latitude},${searchedJob.jobLocations[0].gps.longitude}`,
					workTime : workTime,
					earlyFactor : searchedJob.earlyFactor,
					gapTime: defaultGapTime,
				};
			}
		})
	})

	// Use Google API, where we can insert 24 destinations/24 origins at a time
	const urlObject = {};

	for(let key in jobRoutes) {
		const urlRequests = []
		const searchedJobIds = (Object.keys(jobRoutes[key]))
		let count = 0;
		let index = 0;
		
		let url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${jobRoutes[key]['startPos']}&destinations=`
		searchedJobIds.forEach(id => {
			if (id !== 'startPos') {
				count++;
				url += `${jobRoutes[key][id]['endPos']}|`
			}
			if (count === 24) {
				index++;
				count = 0 ;
				url += `&traffic_model=best_guess&departure_time=now&key=${process.env.GOOGLE_KEY}`;
				urlRequests.push(url)
				url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${jobRoutes[key]['startPos']}&destinations=`;
			}
		})
		url += `&traffic_model=best_guess&departure_time=now&key=${process.env.GOOGLE_KEY}`;

		urlRequests.push(url)

		urlObject[key] = {
			count : index * 24 + count,
			urlRequests : urlRequests
		}
	}


	const scheduledJobOrder = {};

	const jobArrangement = Object.keys(jobRoutes);
	const promiseList = [];

	// Adding edges and secondary check whether jobs are able to reach another job
	for(let origin in urlObject) {
		let count = 0;
		const gapTime = defaultGapTime;
		const objCount = urlObject[origin]['count'];
		const urls = urlObject[origin]['urlRequests'];
		const searchedIds = Object.keys(jobRoutes[origin])

		urls.forEach(url => {
			// console.log(url)
			const request = fetch(url, {
				method : "POST",
				headers : { "content-type" : "application/json" } 
			})
			.then(results => {
				return results.json()
			})
			.then(results => {
				if (results.status === 'OK') {
					const routes = results['rows'][0]['elements']
					routes.forEach((items, i) => {
						const destination = searchedIds[count+1]
						const relatedObj = jobRoutes[origin][destination]
						const earlyFactor = relatedObj.earlyFactor
						const workTime = relatedObj.workTime
						count++;
						// console.log(items)


						if(items.status === "OK") {
							// Round job to the nearest minute
							let travelTime = ((items.duration_in_traffic.value)/60);
							if (travelTime < 0.5) {
								travelTime = 1;
							} else {
								travelTime = Math.round(travelTime)
							}

							// Check whether travel time is less than the gap time plus the leniancy value
							const leniancyValue = gapTime + leniancy
							// console.log(destination)
							// if(destination === "4f50da99-c1dc-48bb-a51d-70c948a7f8ed" && origin === "StartNode") {
							// 	console.log(travelTime)
							// 	console.log(leniancyValue)
							// }

							// if (leniancyValue < travelTime) {
								// console.log(`${origin} cannot make it to ${destination} in time`)
							// } else { 
								// console.log(travelTime)
								try{
									const fromJob = jobArrangement.indexOf(origin)
									const toJob = jobArrangement.indexOf(destination)
									const earlyMultiplier = 1 + ((jobArrangement.length-earlyFactor)/100)
									const weighting = travelTime * -1 * earlyMultiplier
									scheduledJobOrder[fromJob][toJob] = weighting

									// graph.addEdge(new jsgraphs.Edge(fromJob, toJob, weighting));
								} catch(e) {
									const fromJob = jobArrangement.indexOf(origin)
									const toJob = jobArrangement.indexOf(destination)
									const earlyMultiplier = 1 + ((jobArrangement.length-earlyFactor)/100)
									const weighting = travelTime * -1 * earlyMultiplier
									scheduledJobOrder[fromJob] = {}
									scheduledJobOrder[fromJob][toJob] = weighting
									// console.log(e)
								}
							// }
						}
					})
				}
			})
			.catch(e => {
				console.log(e)
			})
			promiseList.push(request)
		})
	}

	Promise.all(promiseList).then(result => {

		// Get indexes of jobs and store them
		const plannedJobIndex = []



		plannedJobs.forEach((job,i) => {
			if(plannedJobs[i].id !== "EndNode") {
				const jobA = jobArrangement.indexOf(plannedJobs[i].id)
				plannedJobIndex.push(jobA)

			}
		})


		plannedJobIndex.push(jobArrangement.indexOf("EndNode"))

		// Remove edges from planned jobs to other planned jobs
		for(let origin in scheduledJobOrder) {
			for(let destination in scheduledJobOrder[origin]) {
				if (plannedJobIndex.includes(parseInt(origin))) {
					const indexJob = plannedJobIndex.indexOf(parseInt(origin))
						// Last index represents the endNode
						if(indexJob !== plannedJobIndex.length-1) {
							const nextJob = plannedJobIndex[indexJob + 1]
							if(nextJob.toString() !== destination && (plannedJobIndex.includes(parseInt(destination)))) {
								delete scheduledJobOrder[origin][destination]
							}
						} else {
						// Remove all nodes associated with the endNode
						delete scheduledJobOrder[origin]
					}
				} else if (destination === origin) {
					delete scheduledJobOrder[origin][destination]
				}
			}
		}


		jobArrangement.forEach((id, i) => {
			console.log(`${i} ==> ${id}`)
		})

		console.log(scheduledJobOrder['42'])

		// Create object containing the index of jobs and the time gap between each planned jobs
		const plannedJobGaps = [];
		plannedJobs.forEach((job,i) => {
			if(plannedJobs[i].id !== "EndNode") {
				const gapTime = moment(new Date(plannedJobs[i+1].plannedFrom)).diff(moment(new Date(plannedJobs[i].plannedTo)), 'minutes')
				const jobA = jobArrangement.indexOf(plannedJobs[i].id)
				const jobB = jobArrangement.indexOf(plannedJobs[i+1].id)
				const gapObj = {
					indexIdA : jobA,
					indexIdB : jobB,
					jobIdA : plannedJobs[i].id,
					jobIdB : plannedJobs[i+1].id,
					gapTime : gapTime
				}
				plannedJobGaps.push(gapObj)
			}
		})

		//
		const scheduledJobIndex = []

		scheduledJobs.forEach(job => {
			const jobIndex = jobArrangement.indexOf(job.id)
			scheduledJobIndex.push(jobIndex)
		})

		const searchedNodes = []
		const idealRouteIndex = [];
		const travelTimes = [];

		/*
			Performs comparison between the edges to find the lowest weighting, once a node has been selected as the node with the lowest weighting,
			this node is excluded from further comparisons and is used to the next possible set of nodes.
		*/

		const workLeniancy = defaultWorkTime + defaultGapTime

		plannedJobGaps.forEach(item => {

			// 55 is a static value, 45 minutes for the actual job, 10 minute leniancy
			let jobNumber = Math.floor(item.gapTime/workLeniancy)
			// console.log(jobNumber)
			let currentNode = item.indexIdA;
			idealRouteIndex.push(currentNode)
			if(jobNumber !== 0) {
				const scheduledOrder = [];
				while(jobNumber > 1 ) {
				// for(let i = 0; i < jobNumber; i++) {
					for(let origin in scheduledJobOrder){
						if(parseInt(origin) === currentNode){
							console.log(currentNode)
							const paths = Object.entries(scheduledJobOrder[origin])
							const indexPath = [];
							const weightPath = []
							paths.forEach(path => {
								console.log(path)
								if(!plannedJobIndex.includes(parseInt(path[0])) && !searchedNodes.includes(parseInt(path[0]))){
									indexPath.push(path[0])
									weightPath.push(path[1]*-1)
								}
							})
							if (indexPath.length !== 0 || weightPath.length !== 0){
								const indexOfLowest = weightPath.indexOf(Math.min.apply(Math, weightPath))
								travelTimes.push(Math.min.apply(Math, weightPath))
								idealRouteIndex.push(parseInt(indexPath[indexOfLowest])) 
								searchedNodes.push(parseInt(indexPath[indexOfLowest])) 
								console.log(`${currentNode} -> ${indexPath[indexOfLowest]} in ${(Math.min.apply(Math, weightPath))}min(s)`)
								currentNode = parseInt(indexPath[indexOfLowest]);


							}
							item.gapTime -= (defaultWorkTime + defaultGapTime)
							jobNumber = Math.floor(item.gapTime/workLeniancy)
							// console.log(jobNumber)
						}
					}
				}
			}
		})

		// console.log(plannedJobIds)
		// console.log(plannedJobIndex)
		// console.log(scheduledJobOrder)

		// console.log(travelTimes)

		idealRouteIndex.push(plannedJobGaps[plannedJobGaps.length-1].indexIdB)

		const idealRouteIds = []

		idealRouteIndex.forEach(index => {
			idealRouteIds.push(jobArrangement[index])
		})

		const idealRouteObj = [];

		// Find the objects relating to the path found
		idealRouteIds.forEach(id => {
			scheduledAndPlannedJobs.forEach(job => {
				if(job.id === id) {
					const obj = {
						id : job.id,
						description : job.description,
						plannedFrom : job.plannedFrom,
						plannedTo : job.plannedTo,
						scheduleFrom : job.scheduleFrom,
						scheduleTo : job.scheduleTo,
						assignedUserId : job.assignedUserId,
						latitude: job.jobLocations[0].gps.latitude,
						longitude: job.jobLocations[0].gps.longitude
					}
					idealRouteObj.push(obj)
				}
			})
		})

		// For the path, if the job is a scheduled job, take the previous job's plannedTo time and add 45 minutes to it
		idealRouteObj.forEach((job, i) => {
			if((job.plannedFrom === 'null.000Z' || job.plannedTo === 'null.000Z' || job.plannedTo === job.plannedFrom) 
				&& job.id !== "EndNode" && job.id !== "StartNode") {
				job.plannedFrom = idealRouteObj[i-1].plannedTo;
			const startTime = job.plannedFrom;
			const endTime = new Date(startTime)
				// 2700000 = 45minutes, 600000 = 10minutes
				endTime.setTime(endTime.getTime() + defaultWorkTime * 60000)
				job.plannedTo = endTime.toISOString();
				job.scheduledBool = true
			} else {
				job.scheduledBool = false
			}
		})

		/* 
			For the path found, if the previous job is a scheduled job, then the current job is a schedule job, 
			add 10 minutes between them to create the 10 minute gap time
		*/
		let travelTimeCount = 0;
		let travelTimeValue = 0;
		console.log(travelTimes)

		idealRouteObj.forEach((job, i) => {
			if(job.id !== "StartNode"){
				if(idealRouteObj[i-1].id === "StartNode") {
					console.log(travelTimes[0])
				}
				if(job.scheduledBool === true && idealRouteObj[i-1].scheduledBool === false) {
					const timeGapStart = new Date(job.plannedFrom);
					const timeGapEnd = new Date(job.plannedTo);
					// Add 10 minute time gap
					timeGapStart.setTime(timeGapStart.getTime() + travelTimes[travelTimeCount] * 60000)
					timeGapEnd.setTime(timeGapEnd.getTime() + travelTimes[travelTimeCount] * 60000)
					job.plannedFrom = timeGapStart.toISOString();
					job.plannedTo = timeGapEnd.toISOString();
					travelTimeValue+= travelTimes[travelTimeCount]
					travelTimeCount++;
				} else if (job.scheduledBool === true && idealRouteObj[i-1].scheduledBool === true) {
					const timeGapStart = new Date(job.plannedFrom);
					const timeGapEnd = new Date(job.plannedTo);
					// Add 10 minute time gap
					timeGapStart.setTime(timeGapStart.getTime() + (travelTimes[travelTimeCount] + travelTimeValue) * 60000)
					timeGapEnd.setTime(timeGapEnd.getTime() + (travelTimes[travelTimeCount] + travelTimeValue) * 60000)
					job.plannedFrom = timeGapStart.toISOString();
					job.plannedTo = timeGapEnd.toISOString();
					travelTimeValue+= travelTimes[travelTimeCount]
					travelTimeCount++;
				}
			}
		})

		// Prints out job path in console

		// console.log('')
		// console.log('//////////////////////////////////')
		// idealRouteObj.forEach(job => {
		// 	console.log(`Job Id: ${job.id}`)
		// 	console.log(`Job Description: ${job.description}`)
		// 	console.log(`${new Date(job.plannedFrom)} ==> ${new Date(job.plannedTo)}`)
		// 	console.log(`Assigned To : ${job.assignedUserId}`)
		// 	console.log('//////////////////////////////////')
		// })

		// Create route to return in JSON

		let fullPath = [];
		if(idealRouteObj.length > 2 && routeChoice === "singleRoute") {
			idealRouteObj.forEach((job,i) => {
				if(i < 2) {
					const jobObject = {
						order : i+1,
						id: job.id,
						description : job.description,
						plannedFrom : job.plannedFrom,
						plannedTo : job.plannedTo,
						scheduled : job.scheduledBool,
						scheduleFrom : job.scheduleFrom,
						scheduleTo : job.scheduleTo,
						assignedTo : job.assignedUserId,
						latitude: job.latitude,
						longitude: job.longitude
					}
					fullPath.push(jobObject)
				}	
			})
		} else {
			fullPath = idealRouteObj.map((job,i) => {
				// console.log(job)
	
				const jobObject = {
					order : i+1,
					id: job.id,
					description : job.description,
					plannedFrom : job.plannedFrom,
					plannedTo : job.plannedTo,
					scheduled : job.scheduledBool,
					scheduleFrom : job.scheduleFrom,
					scheduleTo : job.scheduleTo,
					assignedTo : job.assignedUserId,
					latitude: job.latitude,
					longitude: job.longitude
				}
				return jobObject
			})
		}

		// console.log(fullPath)
		console.log("\x1b[32m","STATUS: A recommended path was found")
		console.log("\x1b[0m")
		const returnObj = {
			status_message : "A recommended path was found",
			recommended_path : fullPath,
			total_jobs : totalJobSize,
			jobs_within_range : withinRangeSize,
			planning_routes_possible : routesPossible,
			total_scheduled_jobs : scheduledJobCount
		}
		res.status(200).json(returnObj)

	})
}


module.exports = {
	createRoutes : createRoutes
}