const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
require("dotenv").config();
const fetch = require("node-fetch");
const moment = require("moment");
const jsgraphs = require("js-graph-algorithms");
const routes = require("./plannedRoutes");
moment().format();

const app = express();
app.use(bodyParser.json());
app.use(cors());
////////////////////////////////////////////////////

activeParams = [];

////////////////////////////////////////////////////

const userList = [];

////////////////////////////////////////////////////

let token;
let dataUrl;
let dataCred;

////////////////////////////////////////////////////

// Leniency value to allow to slight delays (i.e. finding parking, getting equipment, etc)
let leniancy = 10;

// Default work time is the time for all scheduled jobs until Estimated Duration is fixed
let defaultWorkTime = 45;

// Default gap time is the time between each scheduled jobs as a default
let defaultGapTime = 10;

// Default radius is the radius of which to search jobs, jobs within the radius are chosen
let radius = 40;

////////////////////////////////////////////////////

// True to enable testing with demo data, false to enable testing with Con-X production data
let demo = false;

// True to ensure jobs are only for the user or null, false to allow all jobs to be possible to access (TESIING AND DEMO PURPOSE ONLY)
let userRestrict = true;

// True to allow any user to access API, False to restrict invalid user from accessing API
let userBypass = true;

// True to restrict location, false to allow any jobs
let locationRestrict = true;

////////////////////////////////////////////////////

/*
    Gets the ideal path 
    @params {Object} req - Input to the API
    @params {Object} res - Output from the API
    @params {String} user - The user id
    @params {Object} location - Object containing the latitude and longitude of the requester
    @returns {Promise} Returns promise which resolves the ideal path
*/

function sortJobs(
    req,
    res,
    user,
    location,
    routeChoice,
    excludedIds
) {
    let currentTime;
    let restrictTime;
    let urlExtension;

    // Set which domain to get the data from

    if (demo) {
        dataUrl = "https://demo.api.con-x.net";
        dataCred = {
            userName: process.env.DEMO_USER,
            password: process.env.DEMO_PASSWORD
        };
    } else {
        dataUrl = "https://wells.con-x.io";
        dataCred = {
            userName: process.env.PRODUCT_USER,
            password: process.env.PRODUCT_PASSWORD
        };
    }

    console.log("////////////////////////////////");
    console.log(`Leniancy : ${leniancy}`);
    console.log(`Default Work Time : ${defaultWorkTime}`);
    console.log(`Default Gap Time : ${defaultGapTime}`);
    console.log(`Demo : ${demo}`);
    console.log(`User Restrict : ${userRestrict}`);
    console.log(`Location Restrict : ${locationRestrict}`);
    console.log(`Radius : ${radius} km`);
    console.log("////////////////////////////////");

    if (demo) {
        currentTime = moment("27-1-2019", "DD-MM-YYYY");
        restrictTime = moment("27-1-2019", "DD-MM-YYYY");
    } else {
        currentTime = moment.utc();
        restrictTime = moment.utc();
    }

    restrictTime.add(1, "days");

    if (userRestrict) {
        urlExtension = `/odata/v1/Jobs?___filter=(plannedTo 
        gt plannedFrom and (plannedFrom gt ${currentTime.toISOString()} and plannedTo lt ${restrictTime.toISOString()}) 
        or (scheduleTo gt ${restrictTime.toISOString()} and scheduleFrom lt ${currentTime.toISOString()}) and (assignedUserId eq null or assignedUserId eq '${user}')
        and currentStatus/isCompletedStatus eq false)`;

    workflowKey.forEach((key, i) => {
      if (i === 0) {
        urlExtension += ` and (workflow/key eq '${key}'`;
      } else {
        urlExtension += ` or workflow/key eq '${key}'`;
      }
    });

    if (workflowKey.length !== 0) {
      urlExtension += ")";
    }
    urlExtension += `&___expand=jobLocations($expand=gps),currentStatus,workflow&___orderby=plannedFrom`;
  } else {
    urlExtension = `/odata/v1/Jobs?___filter=(plannedTo 
        gt plannedFrom and (plannedFrom gt ${currentTime.toISOString()} and plannedTo lt ${restrictTime.toISOString()}) 
        or (scheduleTo gt ${restrictTime.toISOString()} and scheduleFrom lt ${currentTime.toISOString()})
        and currentStatus/isCompletedStatus eq false)`;

    workflowKey.forEach((key, i) => {
      if (i === 0) {
        urlExtension += ` and (workflow/key eq '${key}'`;
      } else {
        urlExtension += ` or workflow/key eq '${key}'`;
      }
    });
    if (workflowKey.length !== 0) {
      urlExtension += ")";
    }

    urlExtension += `&___expand=jobLocations($expand=gps),currentStatus,workflow&___orderby=plannedFrom`;
  }

  // console.log(urlExtension)
  return fetch(`${dataUrl}/v1/Account/Login`, {
    method: "POST",
    headers: { "Content-type": "application/json" },
    body: JSON.stringify(dataCred)
  })
    .then(response => {
      return response.json();
    })
    .then(response => {
      token = response;
    })
    .then(response => {
      fetch(`${dataUrl}${urlExtension}`, {
        method: "GET",
        headers: {
          "Content-type": "application/json",
          Authorization: "Bearer " + token
        }
      })
        .then(result => {
          return result.json();
        })
        .then(result => {
          const filteredJobs = result.filter(job => {
            if (excludedIds.includes(job.id)) {
              return false;
            } else {
              return true;
            }
          });

          if (result.length === 0) {
            console.log("\x1b[31m", "ERROR: No jobs available");
            const returnObj = {
              status_message: "No jobs available"
            };
            console.log("\x1b[0m");
            res.status(401).json(returnObj);
          } else {
            routes.createRoutes(
              fetch,
              filteredJobs,
              moment,
              demo,
              leniancy,
              jsgraphs,
              req,
              res,
              user,
              location,
              userRestrict,
              locationRestrict,
              defaultWorkTime,
              defaultGapTime,
              routeChoice,
              radius
            );
          }
        });
    });
}

/* 
    Verifies user ID 
    @params {Object} req - Input to the API
    @params {Object} res - Output from the API
*/

function bestJob(req, res) {
  let { user, location, routeChoice, excludedIds } = req.body;

  activeParams.forEach((param, i) => {
    if (
      param["Parameter KEY"] === "TIME-LN" &&
      (param["Static Value"] != "" || param["Static Value"] != "NONE")
    ) {
      leniancy = parseInt(param["Static Value"]);
    }
    if (
      param["Parameter KEY"] === "TIME-EST" &&
      (param["Static Value"] != "" || param["Static Value"] != "NONE")
    ) {
      defaultWorkTime = parseInt(param["Static Value"]);
    }
    if (
      param["Parameter KEY"] === "TIME-GAP" &&
      (param["Static Value"] != "" || param["Static Value"] != "NONE")
    ) {
      defaultGapTime = parseInt(param["Static Value"]);
    }
    if (
      param["Parameter KEY"] === "USER-ID" &&
      (param["Static Value"] != "" || param["Static Value"] != "NONE")
    ) {
      user = param["Static Value"];
    }
    if (
      param["Parameter KEY"] === "DEBB-RAD" &&
      (param["Static Value"] != "" || param["Static Value"] != "NONE")
    ) {
      radius = parseInt(param["Static Value"]);
    }
    if (
      param["Parameter KEY"] === "DEBB-WORK" &&
      (param["Static Value"] != "" || param["Static Value"] != "NONE")
    ) {
      workflowKey = param["Static Value"];
    }
  });

  // Extra checks in case of missing parameters

  // If no workflowKey parameter found
  if (workflowKey === undefined) {
    workflowKey = [];
    // If workflowKey isn't an array
  } else if (!Array.isArray(workflowKey)) {
    workflowKey = [];
  }

  if (excludedIds === undefined) {
    excludedIds = [];
  } else if (!Array.isArray(excludedIds)) {
    excludedIds = [];
  }

  // If no routeChoice parameter found
  if (routeChoice === undefined) {
    routeChoice = "fullRoute";
    // If routeChoice parameter is invalid
  } else if (routeChoice !== "singleRoute" && routeChoice !== "fullRoute") {
    routeChoice = "fullRoute";
  }

  // If location parameter is not found or values of lat and long are invalid
  let locationError = false;
  if (isNaN(location.latitude) || isNaN(location.longitude)) {
    locationError = true;
  }

  if (locationError) {
    console.log("\x1b[31m", "ERROR: Invalid user location");
    const returnObj = {
      status_message: "Invalid user location"
    };
    console.log("\x1b[0m");
    res.status(401).json(returnObj);
  } else {
    sortJobs(req, res, user, location, routeChoice, excludedIds);
  }
}

function getDocsParams(req, res) {
  const link = `https://sheetdb.io/api/v1/ult4bplkzu30i`;
  fetch(link, {
    method: "GET",
    headers: {
      "Content-type": "application/json"
    }
  })
    .then(response => {
      return response.json();
    })
    .then(response => {
      response.forEach(param => {
        if (param.ACTIVE == "Y") {
          activeParams.push(param);
        }
      });
      bestJob(req, res);
    });
}

// API calls

app.post("/bestJob", (req, res) => {
  getDocsParams(req, res);
});

app.get("/", (req, res) => {
  res.send("Hellow World");
});

//const port = process.env.PORT || 30031;
const port = 8080;

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
